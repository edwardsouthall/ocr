#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 14 13:53:31 2019

@author: sydney
"""

import numpy as np
import cv2

from train_digits import *
from common import mosaic

print("Loading svm model")
model = cv2.ml.SVM_load('digits_svm.dat') #!!Opencv 3.0 beta bug cannot load svm file in python with model.load, so this is a workaround

print('Loading digits from digits.png ... ')
# Load data.
digits, labels = load_digits('digits.png')

print('Shuffle data ... ')
# Shuffle data
rand = np.random.RandomState(10)
shuffle = rand.permutation(len(digits))
digits, labels = digits[shuffle], labels[shuffle]

print('Deskew images ... ')
digits_deskewed = list(map(deskew, digits))
    
print('Defining HoG parameters ...')
# HoG feature descriptor
hog = get_hog();

print('Calculating HoG descriptor for every image ... ')
hog_descriptors = []
for img in digits_deskewed:
    hog_descriptors.append(hog.compute(img))
hog_descriptors = np.squeeze(hog_descriptors)

resp = model.predict(hog_descriptors)
