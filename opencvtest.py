
import numpy as np
import cv2

# Load a colour image in grayscale.
img = cv2.imread('lenna.jpeg', 0)

cv2.imshow('image', img)
cv2.waitKey(0)
cv2.destroyAllWindows()